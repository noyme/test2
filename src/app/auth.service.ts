import { CitiesService } from './cities.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  
  user:Observable<User | null>;


  login(email:string, password:string){
    return this.afAuth.signInWithEmailAndPassword(email, password)
  }

  getUser():Observable<User | null> {
    return this.user; 
  } 

  signUp(email:string, password:string) {
      return this.afAuth.createUserWithEmailAndPassword(email, password);

  }

  logout() {
    this.afAuth.signOut();
  }

  constructor(private afAuth:AngularFireAuth , private router:Router, private citiesService:CitiesService  ) {
    this.user = this.afAuth.authState; 
  }
}
