export interface City {
    name:string,
    id?:string,
    temperature?:any,
    img?:any,
    humidity?:number,
    result?: string,
    saved?:boolean,
    new?:boolean
}
