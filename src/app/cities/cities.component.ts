import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../auth.service';
import { CitiesService } from './../cities.service';
import { City } from './../interfaces/city';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {

  displayedColumns: string[] = ['name', 'get', 'temperature',  'humidity','img', 'predict', 'result'];
  temperature:number; 
  image:string; 
  lon:number; 
  lat:number;
  country:string; 
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  cities:City[];
  cities$;
  userId:string;
  city;
  predictButt = [];
  saved = [];
  new = [];

  constructor(private citiesService:CitiesService, private authService:AuthService,
    private route:ActivatedRoute, private http:HttpClient, private db:AngularFirestore) { }

  getData(i){ 
    const city = this.cities[i].name
    this.weatherData$ = this.citiesService.searchWeatherData(city); 
    this.weatherData$.subscribe(
      data => {
        this.temperature = data.temperature;
        this.image = data.image;
        this.country = data.country;
        this.lon = data.lon;
        this.lat = data.lat;
        this.cities[i].humidity = data.humidity;
        this.cities[i].temperature = this.temperature; 
        this.cities[i].img = this.image;
        this.predictButt[i] = true;
 
      }, 
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message; 
      }
    )
  }
  predict(index){
    this.cities[index].result = 'Will difault';
    this.citiesService.predict(this.cities[index].temperature, this.cities[index].humidity).subscribe(
      res => {console.log(res);
        if(res >= 0.5){
          var result = 'Will be rainy';
        } else {
          var result = 'Will not be rainy'
        };
        console.log(result);
        this.cities[index].result = result}
        
    ); 
    this.saved[index] =true;
  }

  updateCity(index){
    this.saved[index] = true;
    this.cities[index].new = true;
    this.citiesService.updateRsult(this.userId,this.cities[index].id,this.cities[index].temperature, this.cities[index].humidity, this.cities[index].img, this.cities[index].result, this.cities[index].saved);
  }

  newData(index){
    this.db.doc(`users/${this.userId}/cities/${this.cities[index].id}`).update(
      {
        temperature:null,
        humidity:null,
        img:null,
        result:null,
        saved:false
      });
    this.saved[index] = false;
    this.getData(index);

  }

 



  ngOnInit(): void {
    
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.cities$ = this.citiesService.getCities(this.userId);
        
        this.cities$.subscribe(
          docs => {
            this.cities = [];
            for (let document of docs){
              const city:City = document.payload.doc.data();
              city.id = document.payload.doc.id;
              this.cities.push(city);
            }
          }
        )
      }
    )
    
    


  }
  }


