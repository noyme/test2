import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { CitiesService } from '../cities.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  email:string;
  password:string;
  userId;



  constructor(public auth:AuthService, private router:Router, private citiesService:CitiesService) { }

  onSubmit(){
    this.auth.signUp(this.email, this.password)
    .then((result) => {
      window.alert("You have been successfully registered!");
      console.log(result.user);
   
      this.router.navigate(['/welcome']);
      this.auth.getUser().subscribe(
        user => {
          this.userId = user.uid;
      console.log(user.uid);
      this.citiesService.addCity(user.uid);});
      const userId = this.auth.getUser()
      
      }).catch((error) => {
        window.alert(error.message)
      });
    
  }

  ngOnInit(): void {
  }

}
