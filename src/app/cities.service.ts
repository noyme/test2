import { Router } from '@angular/router';
import { City } from './interfaces/city';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  citiesCollection:AngularFirestoreCollection;

  handleError;

  private URL = "https://api.openweathermap.org/data/2.5/weather?q="; 
  private KEY = "c2addb75777e24415bc0705646e8a194"; 
  private IMP = "units=metric"; 


  addCity(userId){
    const london:City = {name:"London"};
    this.userCollection.doc(userId).collection(`cities`).add(london);
    const paris:City = {name:"Paris"};
    this.userCollection.doc(userId).collection(`cities`).add(paris);
    const tel:City = {name:"Tel Aviv"};
    this.userCollection.doc(userId).collection(`cities`).add(tel);
    const jer:City = {name:"Jerusalem"};
    this.userCollection.doc(userId).collection(`cities`).add(jer);
    const berlin:City = {name:"Berlin"};
    this.userCollection.doc(userId).collection(`cities`).add(berlin);
    const rome:City = {name:"Rome"};
    this.userCollection.doc(userId).collection(`cities`).add(rome);
    const dubai:City = {name:"Dubai"};
    this.userCollection.doc(userId).collection(`cities`).add(dubai);
    const ath:City = {name:"Athens"};
    this.userCollection.doc(userId).collection(`cities`).add(ath);

    this.router.navigate(['/welcome']);
  }

  getCities(userId):Observable<any[]>{
    this.citiesCollection = this.db.collection(`users/${userId}/cities`);
    return this.citiesCollection.snapshotChanges()
  }

  updateRsult(userId,id,temperature,humidity, img, result,saved){
    this.db.doc(`users/${userId}/cities/${id}`).update(
      {
        temperature:temperature,
        humidity:humidity,
        img:img,
        result:result,
        saved:true
      })
    }

    newData(userId,id){
      this.db.doc(`users/${userId}/cities/${id}`).update(
        {
          temperature:null,
          humidity:null,
          img:null,
          result:null,
          saved:false
        })
    }

  

  url1 = 'https://6vq6kaqwbk.execute-api.us-east-1.amazonaws.com/beta'; 
  
  predict(temp:number, hium:number):Observable<any>{
    let json = {
      "data": 
        {
          "temp": temp,
          "hium": hium
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url1,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  constructor(private router:Router, private db:AngularFirestore, private http:HttpClient) { }

  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }

  private transformWeatherData(data:WeatherRaw):Weather{
    return {
      name:data.name,
      country:data.sys.country,
      image:`https://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp, 
      lat:data.coord.lat,
      lon:data.coord.lon,
      humidity:data.main.humidity
    } 
  }
}
