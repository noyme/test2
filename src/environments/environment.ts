// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDHGM48_2tv_8EWZsw6ZhHuhuAOfyTRW_M",
    authDomain: "test2-3b311.firebaseapp.com",
    projectId: "test2-3b311",
    storageBucket: "test2-3b311.appspot.com",
    messagingSenderId: "613183303159",
    appId: "1:613183303159:web:0993666fcc9c38e6dbd2f2",
    measurementId: "G-VW3L21GWPB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
